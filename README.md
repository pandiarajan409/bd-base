Brown Dog Base
==============
The Brown Dog base development environment with dockerized versions of each of the services required to setup and work against the Brown Dog Data Transformation Service.
Uses the unix tmux utility to spawn the terminals of each service in a seperate window.
Useful for developers building extractors and converters.

bd
--
Start Brown Dog DTS: the Fence API gateway, Clowder, one extractor, Polyglot, one converter, and the BD command line interface.
To utilize the Brown Dog command line interface in the bottom pane utilize the following default system values:

```
Server: http://fence:8080
Username: fence
Password: testing
```

For example:

```
bd -o bmp -b http://fence:8080 sample.jpg 
Brown Dog API: fence:8080
Username: fence
Password: 
Key: a40844d9-6346-410a-b968-882992c7198a
Token: dc1b5e2b-7d36-4207-863f-db520cc11a05
sample.bmp
```

converter
---------
Start base converter development environment (including Java, SoftwareServer, etc).

extractor
---------
Start base extractor development environment (including Python, PyClowder2, etc).
