#!/bin/bash

#Stop all previous docker containers
docker stop $(docker ps -a -q) &> /dev/null
docker rm -f $(docker ps -a -q) &> /dev/null

PROJECT=browndog
FenceContainerName="fence"
ClowderContainerName="clowder"
ExtractorContainerName="extractors-tesseract"
PolyglotContainerName="polyglot"
ConverterContainerName="converters-imagemagick"
MongodContainerName="mongo"
BDCLIContainerNAME="bdcli"
RABBITContainerName="rabbitmq"

getIDS() {
	FenceID=$(docker ps -aqf "name=$FenceContainerName")
	ClowderID=$(docker ps -aqf "name=$ClowderContainerName")
	ExtractorID=$(docker ps -aqf "name=$ExtractorContainerName")
	PolyglotID=$(docker ps -aqf "name=$PolyglotContainerName")
	ConverterID=$(docker ps -aqf "name=$ConverterContainerName")
	MongodID=$(docker ps -aqf "name=$MongodContainerName")
	BDCLIID=$(docker ps -aqf "name=$BDCLIContainerNAME")
	RABBITID=$(docker ps -aqf "name=$RABBITContainerNAME")
	CHECKIDS=($FenceID $ClowderID $ExtractorID $PolyglotID $ConverterID $BDCLIID $MongodID $RABBITID)
}

#Start containers

# start redis
docker-compose up -d redis
docker-compose up -d rabbitmq
docker-compose up -d mongo

#Get local IP address
if [[ `uname -s` = *"Linux"* ]]; then
	HOSTIP=`ip route get 1 | awk '{print $NF;exit}'`
else
	HOSTIP=`ifconfig eth0|awk '/inet / {print $2}'|sed 's/addr://'`
fi

if [ -z "$HOSTIP" ]; then
  HOSTIP=`ifconfig en0|awk '/inet / {print $2}'`

  if [ -z "$HOSTIP" ]; then
    echo -e -n "\nLocal Public IP (not 127.0.0.1) : "
    read HOSTIP
  fi
fi

echo "Local Public IP: $HOSTIP"

# wait rabbitmq to be ready
until nc -z ${HOSTIP} 15672
do
    sleep 1
done

# wait mongo to be ready
until nc -z ${HOSTIP} 27017
do
    sleep 1
done

# start clowder after rabbitmq, mongo are ready
docker-compose up --no-deps -d clowder

if [[ `uname -s` = *"Linux"* ]]; then
    echo "Running on Linux"
    CONNECTED=$(netstat -plnt 2>/dev/null | grep ':9000')
else
    echo "Running on Mac"
    CONNECTED=$(lsof -i -n -P | grep TCP 2>/dev/null | grep ':9000')
fi

while [ -z "$CONNECTED" ]; do
    echo $CONNECTED
    sleep 1
done

while ! curl -s http://${HOSTIP}:9000/api/status; do
  echo "clowder is not ready, waiting one second" >&2
  sleep 1
done |
  until grep -q "services.mongodb.MongoDBSecureSocialUserService"; do
    echo "Waiting for pattern to match"
  done > /dev/null

docker-compose up --no-deps -d extractors-tesseract
docker-compose up --no-deps -d polyglot
docker-compose up --no-deps -d fence
docker-compose up --no-deps -d bdcli
docker-compose up --no-deps -d converters-imagemagick

getIDS

sleep 10

#Split the screen
SESSION="BrownDog"
tmux -2 new-session -d -s $SESSION
tmux new-window -t $SESSION:1 -n 'Clowder/Polyglot'

#Fence pane
tmux select-pane -t 0
tmux send-keys "docker logs -f $FenceID" C-m

#Middle pane
tmux split-window -v
tmux resize-pane -U 15
tmux split-window -v

tmux resize-pane -U 4
tmux split-window -v
tmux resize-pane -D 4
tmux select-pane -t 3

#BDCLI pane
tmux select-pane -t 3
tmux send-keys "docker exec -it -u bdcli $BDCLIID bash" C-m

#Clowder pane
tmux select-pane -t 1
tmux send-keys "docker logs -f $ClowderID && clear" C-m

#OCR Extractor pane
tmux split-window -h
tmux send-keys "docker logs -f $ExtractorID && clear" C-m

#Polyglot pane
tmux select-pane -t 3
tmux send-keys "docker logs -f $PolyglotID && clear" C-m

#ImageMagick pane
tmux split-window -h
tmux send-keys "docker logs -f $ConverterID && clear" C-m

docker run --rm -it -e "ADMIN=true" -e "PASSWORD=testing0909" -e "EMAIL_ADDRESS=admin@test.com" -e "MONGO_URI=mongodb://mongo:27017/clowder" --name create-useraccount --net bdbase_bdbase browndog/create-useraccount

tmux -2 attach-session -t $SESSION

docker stop $(docker ps -a -q) &> /dev/null
docker rm -f $(docker ps -a -q) &> /dev/null

echo "bye bye"
