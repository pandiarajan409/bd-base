Brown Dog TMUX Scripts (Repository version)
===========================================

A set of tmux scripts which will split your terminal into panes starting each of the services needed for the Brown Dog data transformation service.
Assumes you have checked out all the needed service repositories and they exist one directory up.  Useful for developers building extractors and converters.  

bd.sh
-----

Start Brown Dog, both the DTS and DAP spawning Fence, Clowder, one extractor, Polyglot, one converter, and the BD command line interface.

dts.sh
---------

Start the Data Tilling Service spawning Fence, Clowder, one extractor, and the BD command line interface.

dap.sh
---------

Start the Data Access Proxy spawning Fence, Polyglot, one converter, and the BD command line interface. 
