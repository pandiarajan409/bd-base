#!/bin/bash

#Start dependencies
if pgrep "redis-server" > /dev/null
then
    echo "Redis is running."
else
    echo "Starting Redis..."
		redis-server &
fi

#Options
echo -e -n "\nUse Dockerized template converter [y/N]?: "
read template
echo -e -n "\nEnable input pane [y/N]?: "
read input_pane

#Split the screen
SESSION="DAP"
tmux -2 new-session -d -s $SESSION
tmux new-window -t $SESSION:1 -n 'Brown Dog Converter Development'

#Fence pane
tmux select-pane -t 0
tmux send-keys "cd ../../fence && sbt \"run-main edu.illinois.ncsa.fence.Server -log.level=DEBUG\"" C-m

#Middle pane
tmux split-window -v

#Input pane
if [ "$input_pane" == "y" ]; then
	tmux resize-pane -U 10
	tmux split-window -v
	tmux resize-pane -D 5
	tmux select-pane -t 2
	tmux send-keys "cd ../../bdcli && clear" C-m
fi

#Polyglot Steward pane
tmux select-pane -t 1
tmux send-keys "cd ../../polyglot/polyglot && bin/PolyglotRestlet.sh" C-m

#Software Server pane
tmux split-window -h
if [ "$template" == "y" ]; then
	localhost=`ip -4 addr show eth0 | grep -Po 'inet \K[\d.]+'`
	tmux send-keys "docker run -t -i -e "RABBITMQ_URI=amqp://guest:guest@$localhost/%2f" browndog/converters-template" C-m	#Template converter
else
	tmux send-keys "cd ../../polyglot/polyglot && bin/SoftwareServerRestlet.sh" C-m		#Local software server
fi

tmux -2 attach-session -t $SESSION
