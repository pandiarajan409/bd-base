#!/bin/bash

#Start dependencies
if pgrep "redis-server" > /dev/null
then
    echo -e "\nRedis is running..."
else
    echo -e "\nStarting Redis..."
		redis-server &
fi

#Options
echo -e -n "\nExtractor [dbpedia, face, versus, TEMPLATE]?: "
read extractor
echo -e -n "\nUse Dockerized template converter [y/N]?: "
read template
echo -e -n "\nEnable input pane [y/N]?: "
read input_pane

#Split the screen
SESSION="BrownDog"
tmux -2 new-session -d -s $SESSION
tmux new-window -t $SESSION:1 -n 'DTS/DAP'

#Fence pane
tmux select-pane -t 0
tmux send-keys "cd ../../fence && sbt \"run-main edu.illinois.ncsa.fence.Server -log.level=DEBUG\"" C-m

#Middle pane
tmux split-window -v
tmux resize-pane -U 15
tmux split-window -v

#Input pane
if [ "$input_pane" == "y" ]; then
	tmux resize-pane -U 4
	tmux split-window -v
	tmux resize-pane -D 4
	tmux select-pane -t 3
	tmux send-keys "cd ../../bdcli && clear" C-m
fi

#Clowder pane
tmux select-pane -t 1
tmux send-keys "cd ../../clowder/clowder && sbt run" C-m

#Extractor pane
tmux split-window -h

if [ "$extractor" == "dbpedia" ]; then
	tmux send-keys "cd ../../clowder/extractors-dbpedia && python extractor.py" C-m			#DBPedia extractor
elif [ "$extractor" == "face" ]; then
	tmux send-keys "cd ../../clowder/extractors-cv/extractors-opencv/extractors-opencv-faces && face.py" C-m		#Face extractor
elif [ "$extractor" == "versus" ]; then
	tmux send-keys "cd ../../clowder/extractors-versus && startup.sh" C-m								#Versus extractor
else
	localhost=`ip -4 addr show eth0 | grep -Po 'inet \K[\d.]+'`
	tmux send-keys "docker run -t -i -e "RABBITMQ_URI=amqp://guest:guest@$localhost/%2f" browndog/extractors-template" C-m	#Template extractor
fi

#Polyglot Steward pane
tmux select-pane -t 3
tmux send-keys "cd ../../polyglot/polyglot && bin/PolyglotRestlet.sh" C-m

#Software Server pane
tmux split-window -h
if [ "$template" == "y" ]; then
	localhost=`ip -4 addr show eth0 | grep -Po 'inet \K[\d.]+'`
	tmux send-keys "docker run -t -i -e "RABBITMQ_URI=amqp://guest:guest@$localhost/%2f" browndog/converters-template" C-m	#Template converter
else
	tmux send-keys "cd ../../polyglot/polyglot && bin/SoftwareServerRestlet.sh" C-m		#Local software server
fi

tmux -2 attach-session -t $SESSION
